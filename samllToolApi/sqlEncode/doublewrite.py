#!/usr/bin/env python
# -*- coding:utf-8 -*-

def description():
    contentVal = '''
    @des
    嵌套关键字，也叫双写技术,就是给关键字加个壳，绕过简单的关键字词过滤逻辑
    @notes
    用于测试薄弱的防护规则    
    '''
    return contentVal
def sqlEncode(**kwargs):
    cpayload = kwargs['payload']
    cwords = kwargs['words']
    rval = ''
    if cwords.find(",") > 0:
        wlists = cwords.split(",")
        for w in wlists:
            wb = w[0:2]
            we = w[2:]
            rval = wb+w+we
            cpayload = cpayload.replace(w,rval)
    else:
        wb = cwords[0:2]
        we = cwords[2:]
        rval = wb+cwords+we
        cpayload = cpayload.replace(cwords,rval)
    return cpayload


