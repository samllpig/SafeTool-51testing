#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
from tkinter import Pack

reModule = {
    '动态调试模块' : {
        'Debugger': 'PE文件动态调试器',
        'Emulator': 'CPU模拟器'

    },
    '静态分析模块' : {

        'PeInfo': '解析PE文件',
        'PeAnalysis':'PE文件静态分析',
        'HexEdit':"二进制编辑器"
    },
    '计算处理模块' : {
        'Calc': '进制转换及计算'

    },
    '辅助处理模块' : {
        'DisAsmCode':'反汇编二进制代码[shellcode]',
        'GenerateBinaryCode': '汇编代码生成二进制代码',
        'RunAsmCode':'模拟环境运行汇编代码'
    }
}

#路径配置
PATH = os.path.dirname(os.path.realpath(__file__))
EMUAPI_PATH = os.path.join(PATH,"emuapi")
WIN_PATH = os.path.join(PATH,"windows")
ASM_PATH = os.path.join(EMUAPI_PATH,"asm")
NASM_PATH =  os.path.join(ASM_PATH,"nasm.exe")#nasm汇编器放在这个目录
WINDLL_PATH = os.path.join(WIN_PATH,"dll")#kernel32.dll,ntdll.dll,user32.dll放在这个目录

