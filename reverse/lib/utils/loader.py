#!/usr/bin/env python
# -*- coding:utf-8 -*-
import importlib
from importlib.abc import Loader
import hashlib
class DebugerLoader(Loader):
    def __init__(self,fullname,fpath,fdata):
        self.fullname = fullname
        self.path = fpath
        self.data = fdata
    def exec_module(self,module):
        fObj = compile(self.data,self.path,"exec",dont_inherit=True)
        exec(fObj,module.__dict__)
def load_module(codeStr):
    try:
        moduleName = "modules_{0}".format(hashlib.md5(codeStr.encode(encoding="UTF-8")).hexdigest())
        fpath = "path_{0}".format(moduleName)
        dLoader = DebugerLoader(moduleName,fpath,codeStr)
        spec = importlib.util.spec_from_file_location(moduleName,fpath,loader=dLoader)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
        return mod
    except ImportError:
        raise ValueError("动态加载模块出错!")