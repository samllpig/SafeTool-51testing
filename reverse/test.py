#!/usr/bin/env python
# -*- coding:utf-8 -*-
from dapi import debugger

module = {
    'bp':"breakpoint.py",
    #'st':'singlestep.py'
    'st':'sdisasm.py'
}
debug = debugger.debugger(module)
path = "c:\\safeproject\\code\\testc\\demo.exe"
debug.load_64_exe(path)
debug.run64()
