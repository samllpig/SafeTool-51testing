#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import traceback
from sapi import peopt,calc,hexfile
from lib.utils import printer
from lib import settings
from dapi import debugger
from emuapi import emulator

def is_file_exists(fpath):
    result = os.path.exists(fpath)
    return result
def api_pe_hash(fpath):
    hashStr = peopt.pe_get_hash(fpath)
    print(hashStr)
def api_pe_warns_info(fpath):
    warns = peopt.pe_get_warns(fpath)
    print(warns)

def api_pe_headers(fpath):
    peopt.pe_header(fpath)

def api_pe_sections(fpath):
    peopt.pe_sections(fpath)

def api_pe_imp_dll_info(fpath):
    peopt.pe_import_info(fpath)

def api_pe_dll_info(fpath,dll):
    peopt.pe_dll_func(fpath,dll)

def api_pe_extract_string(fpath):
    strings = peopt.pe_extract_strings(fpath)
    lenStr = len(strings)
    printer.info("提取的字符串个数: " + str(lenStr))
    for i in range(0,lenStr):
        if i%4 == 0:
            print(end="\n")
            c = input("c:继续 e:退出>> ")
            if c == "e":
                break
        print(strings[i],end="  ")

def api_pe_extract_string_range(fpath,start,end):
    strings = peopt.pe_extract_strings(fpath)
    lenStr = len(strings)
    count = 0
    print("提取的字符串个数: " + str(lenStr))
    for s in strings[start:end]:
        if (count+1)%4 == 0:
            print(end="\n")
        print(s,end="  ")
        count += 1
def api_pe_find_str_mal(fpath):
    strings = peopt.pe_extract_strings(fpath)
    peopt.pe_strings_find_mal(strings)

def api_pe_find_func_mal(fpath):
    funcs = peopt.pe_funcs(fpath)
    peopt.pe_func_find_mal(funcs)

def api_debug_show_hmodule():
    printer.info("断点事件处理模块:")
    for k,v in settings.BP_HANDLE.items():
        info = k + " : " + v
        printer.test(info)
    printer.info("单步调试处理模块")
    for k,v in settings.SS_HANDLE.items():
        info = k + " : " + v
        printer.test(info)
def api_debug_get_bpmoudle():
    result = []
    for k in settings.BP_HANDLE.keys():
        result.append(k)
    return result
def api_debug_get_ssmoudle():
    result = []
    for k in settings.SS_HANDLE.keys():
        result.append(k)
    return result
def api_debug_run(fpath,hmoudle,bpAddr):
    deb = debugger.debugger(hmoudle)
    deb.load_64_exe(fpath,bAddr=bpAddr)
    deb.run64()
def api_calc_b2d(b):
    return calc.binary_decimal(b)
def api_calc_b2o(b):
    return calc.binary_octal(b)
def api_calc_b2h(b):
    return calc.binary_hex(b)
def api_calc_d2b(d):
    return calc.decimal_binary(d)
def api_calc_d2o(d):
    return calc.decimal_octal(d)
def api_calc_d2h(d):
    return calc.decimal_hex(d)
def api_calc_h2b(h):
    return calc.hex_binary(h)
def api_calc_h2d(h):
    return calc.hex_decimal(h)
def api_calc_h2o(h):
    return calc.hex_octal(h)
def api_calc_badd(a,b):
    return calc.binary_add(a,b)
def api_calc_bsub(a,b):
    return calc.binary_sub(a,b)
def api_calc_bmul(a,b):
    return calc.binary_mul(a,b)
def api_calc_bdiv(a,b):
    return calc.binary_div(a,b)
def api_calc_hadd(a,b):
    return calc.hex_add(a,b)
def api_calc_hsub(a,b):
    return calc.hex_sub(a,b)
def api_calc_hmul(a,b):
    return calc.hex_mul(a,b)
def api_calc_hdiv(a,b):
    return calc.hex_div(a,b)
def api_emu_run_pe_tech():
    emulator.pe_emu_51testing()

def api_emu_run_shellcode_tech():
    emulator.shellcode_emu_51testing()

def api_emu_disasm(shellcode,arch = 'I386'):
    emulator.disam_shellcode_51testing(shellcode,arch)

def api_emu_binCode(code:list):
    binCode = emulator.generate_binary_code(code)
    return binCode
def api_emu_run_binCode(code:list):
    emulator.asmcode_emu(code)

def api_get_asm_explain(code:str):
    explain = emulator.get_asm_explain(code)
    return explain

def api_show_shellcode():
    emulator.show_shellcode_choice()
#0622
def api_pe_rva_raw(path):
    rva = input("请输入Rva值: ")
    rva = int(rva,16)
    raw = peopt.pe_rva_raw(path,rva)
    return hex(raw)
def api_hexedit_show_content(path):
    filename = path.split("\\")[-1]
    printer.test("程序名称: " + filename)
    printer.offset("偏移地址")
    printer.hex(20*" " + "十六进制数据" + 15*" ")
    printer.ascii(3*" "+ "解码ASCII")
    print("")
    hx = hexfile.HexTool(path)
    hx.convert_hexfile()
    hx.calc_offset()
    hx.ascii_from_hex()
    hx.show_hex_content()
def api_pe_info(path,info):
    try:
        peopt.pe_info(path,info)
    except Exception as e:
        traceback.print_exc()