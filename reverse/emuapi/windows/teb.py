#!/usr/bin/env python
# -*- coding:utf-8 -*-

from ctypes import *

class teb_struct(Structure):
    _fields_ = [
            ("ExceptionList", c_uint32),         # fs:00h
            ("StackBase", c_uint32),             # fs:04h
            ("StackLimit", c_uint32),            # fs:08h
            ("SubSystemTib", c_uint32),          # fs:0ch
            ("FiberData", c_uint32),             # fs:10h
            ("ArbitraryUserPointer", c_uint32),  # fs:14h
            ("TebPointerAddr", c_uint32),        # fs:18h
            ("EnvironmentPointer", c_uint32),    # fs:1ch
            ("ClientIdUniqueProcess", c_uint32), # fs:20h process id
            ("ClientIdUniqueThread", c_uint32),  # fs:24h current thread id
            ("RpcHandle", c_uint32),             # fs:28h
            ("TlsStorage", c_uint32),            # fs:2ch
            ("PEBAddress", c_uint32),            # fs:30h
            ("LastErrorValue", c_uint32),        # fs:34h
            ("LastStatusValue", c_uint32),       # fs:38h
            ("CountOwnedLocks", c_uint32),       # fs:3ch
            ("HardErrorMode", c_uint32),         # fs:40h
    ]

#PEB,LDR