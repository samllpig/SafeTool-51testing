#!/usr/bin/env python
# -*- coding:utf-8 -*-
from unicorn import *
from unicorn.x86_const import *

class ArchEmu(object):
    def __init__(self,mode):
        self.mode = mode
    def get_arch_x86(self):
        if self.mode == 32:
            self.mu = Uc(UC_ARCH_X86,UC_MODE_32)
        elif self.mode == 64:
            self.mu = Uc(UC_ARCH_X86,UC_MODE_64)
        else:
            self.mu = None
        return self.mu
