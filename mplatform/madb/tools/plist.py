#!/usr/bin/env python
# -*- coding:utf-8 -*-

def str_list(order:str):
    return order.split()

def list_extend(cmd:list,order:str):
    cmd.extend(order.split())
    return cmd
