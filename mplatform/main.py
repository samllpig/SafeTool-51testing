#!/usr/bin/env python
# -*- coding:utf-8 -*-
from cmd import Cmd
import traceback,sys,os
from madb.tools.printer import info,warn
#import mdrozer.localapi
from settings import module
import madb.localapi as mapi
import mhook.localapi as mhapi
import msafe.localapi as msapi
#import mdrozer.localapi as mdapi

class Cli(Cmd):
    def __init__(self):
        os.system("cls")
        Cmd.__init__(self)
        banner = """
            _    _ _     ___ _                 
  _ __  ___| |__(_) |___| _ \ |_  ___ _ _  ___ 
 | '  \/ _ \ '_ \ | / -_)  _/ ' \/ _ \ ' \/ -_)
 |_|_|_\___/_.__/_|_\___|_| |_||_\___/_||_\___|
 
                """
        self.intro = banner
        self.prompt = 'appTest>> '
    def do_show(self,param):
        for k,v in module.items():
            info(k + ": " + v)
    def do_adb(self,param):
        try:
            param = param.strip()
            paramlist = param.split(".")
            if paramlist[0] == "show":
                if paramlist[1] == "command":
                    mapi.show_command(paramlist[2])
                elif paramlist[1] == "rule":
                    mapi.show_command_rule(paramlist[2])
                elif paramlist[1] == "pid":
                    mapi.show_pid_info(paramlist[2])
            elif paramlist[0] == "exec":
                if paramlist[1] == "id":
                    mapi.adb_execute(paramlist[2])
                elif paramlist[1] == "cust":
                    olist = paramlist[2].split()
                    if olist[1] == "install":
                        olist[2] += ".apk"
                    mapi.adb_cust_execute(olist)
            elif paramlist[0] == "find":
                if paramlist[1] == "pid":
                    mapi.find_spec_pid()
                #elif paramlist[1] == "logcat":
                    #mapi.find_spec_logcat(paramlist[2])
            elif paramlist[0] == "start":
                if paramlist[1] == "shell":
                    mapi.start_adb_shell()
            else:
                warn("命令不对!")
        except Exception as e:
            warn("[MADB]命令格式错了!")
    def do_hook(self,param):
        try:
            param = param.strip()
            paramlist = param.split(".")
            if paramlist[0] == "check":
                if paramlist[1] == "env":
                    mhapi.check_envir()
                else:
                    warn("参数不对!")
            elif paramlist[0] == "show":
                if paramlist[1] == "devices":
                    mhapi.show_list_devices()
                elif paramlist[1] == "forward":
                    mhapi.show_forward()
                elif paramlist[1] == "frida-ps":
                    mhapi.show_frida_ps()
                else:
                    warn("参数不对")
            elif paramlist[0] == "set":
                if paramlist[1] == "connect":
                    mhapi.set_connect_devices(paramlist[2])
                elif paramlist[1] == "frida":
                    if paramlist[2] == "forward":
                        mhapi.set_frida_forward()
                    else:
                        warn("参数不对!")
                else:
                    warn("参数不对!")
            elif paramlist[0] == "exec":
                if paramlist[1] == "51testing":
                    if paramlist[2] == "teach":
                        mhapi.poc_51testingTeach()
                elif paramlist[1] =="myapp":
                    if paramlist[2] == "sumcalc":
                        mhapi.poc_myappSumCalc()
                    elif paramlist[2] == "samefunc":
                        mhapi.poc_myappSameFunc()
                    elif paramlist[2] == "secretfunc":
                        mhapi.poc_myappSecretFunc()
                    elif paramlist[2] == "secretrpc":
                        mhapi.poc_myappRpcSecret()
                    elif paramlist[2] == "textview":
                        mhapi.poc_myappTextView()
                else:
                    warn("参数不对!")
            else:
                warn("参数不对!")
        except Exception as e:
            warn("[MHOOK]异常!")
    def do_safe(self,param):
        try:
            param = param.strip()
            paramlist = param.split(".")
            if paramlist[0] == "enum":
                apkpath = input("请输入apk路径: ").strip()
                if paramlist[1] == "activity":
                    msapi.detect_activity_attacksurface(apkpath)
                elif paramlist[1] == "receiver":
                    msapi.detect_receiver_attacksurface(apkpath)
                elif paramlist[1] == "provider":
                    msapi.detect_provider_attacksurface(apkpath)
                elif paramlist[1] == "service":
                    msapi.detect_service_attacksurface(apkpath)
                else:
                    warn("参数不对![activity,receiver,provider,service]")
            elif paramlist[0] == "show":
                apkpath = input("请输入apk路径: ").strip()
                if paramlist[1] == "permissions":
                    msapi.detect_permissions(apkpath)
                else:
                    warn("参数不对![permissions]")
            else:
                warn("参数不对!")
        except Exception as e:
            warn("[MSAFE]异常")
    def do_mdrozer(self,param):
        try:
            info("mdrozer还需再改改,下个版本.")
        except Exception as e:
            warn("[MDROZER]异常")
    def do_exit(self,param):
        warn("退出!")
        sys.exit()
    def default(self,param):
        print("命令不存在!")

if __name__ == "__main__":
    try:
        cli = Cli()
        cli.cmdloop()
    except Exception as e:
        print(e)
        traceback.print_exc()

