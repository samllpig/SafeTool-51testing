#!/usr/bin/env python
# -*- coding:utf-8 -*-
from madb import adb
from madb.tools.printer import info,plus,warn
from madb.adb.adbpackage import Adb
from madb.adb.settings import command,c_note
from mhook.tools.setting import mobile
import os

adbo = Adb()

def list_devices():
    order = "6"
    adbo.execute(order)

def connect_mumu():
    order = mobile['mumu'].split()
    adbo.customize_execute(order)

def set_frida_forward():
    oo = "adb forward tcp:27042 tcp:27042".split()
    ot = "adb forward tcp:27043 tcp:27043".split()
    adbo.customize_execute(oo)
    adbo.customize_execute(ot)

def set_frida_server():
    order = 'adb shell "/data/local/tmp/frida-server.sh"'.split()
    adbo.customize_execute(order)

def frida_ps():
    order = "frida-ps -U"
    try:
        #adbo.customize_execute(order)
        os.system(order)
    except Exception as e:
        warn("未启动frida-server或者未设置端口转发.")
        
def find_app():
    order = "18"
    adbo.execute(order)

def forward_list():
    order = "12"
    adbo.execute(order)

    