#!/usr/bin/env python
# -*- coding:utf-8 -*-
import yaml
import os
import re
class config:
    def __init__(self):
        path = os.path.dirname(__file__)
        self.yamlPath = path + os.path.sep + "template.yaml"
        rFile = open(self.yamlPath,'r',encoding='utf-8')
        content = rFile.read()
        self.conf = yaml.load(content,Loader=yaml.FullLoader)
    def get_ip(self):
        return self.conf['ip']
    def get_port(self):
        return self.conf['port']
    def set_ip(self,ip):
        self.conf['ip']= ip
        wFile = open(self.yamlPath,'w',encoding='utf-8')
        yaml.dump(self.conf,wFile)
    def set_port(self,port):
        self.conf['port']= port
        wFile = open(self.yamlPath,'w',encoding='utf-8')
        yaml.dump(self.conf,wFile)
