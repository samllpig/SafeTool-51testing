#!/usr/bin/env python
# -*- coding:utf-8 -*-
from http.server import BaseHTTPRequestHandler
import yaml
import os
def nvddata(httpserver:BaseHTTPRequestHandler,params):
    dbpath = "db"
    fname = params.split("/")[-1]
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + dbpath
    flist = os.listdir(fp)
    for f in flist:
        if f == fname:
            fdata = fp + os.sep + fname
            f = open(fdata,"rb")
            resp = f.read()
            f.close()
            httpserver.send_cve_success(resp)
