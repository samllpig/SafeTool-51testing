#!/usr/bin/env python
# -*- coding:utf-8 -*-
APP_PATH = "mysite.app"
#加入新建的app名称
INSTALLED_APP = [
    #"makeWebServer"
    "metasploit",
    "nikto",
    "nmap",
    "dirsearch",
    "xxe",
    "xss",
    "cvedb",
    "csrf"
]
#场景码
CODE_SCENE = {
    '1':'LOGOUT',
    '2':'LOGIN'
}
#识错码
CODE_ERROR = {
    '1':'1000',#数据不存在
    '2':'1001',#签名校验失败
    '3':'1002',#token校验失败
    '4':'1003',#场景不存在
    '5':'1004',#不是json数据
    '6':'1005'#文件不存在
}