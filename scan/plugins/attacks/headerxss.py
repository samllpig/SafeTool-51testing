#!/usr/bin/env python
# -*- coding:utf-8 -*-

from re import search,I
from lib.utils.params import *
from lib.utils.printer import *
from lib.request.request import *
from lib.utils.payload import *

class headerxss(Request):
    """
    HTTP Headers头信息中XSS漏洞
    """
    get = "GET"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'headerxss':None
        }

    def cookie(self):
        isNothing = True
        for payload in pxss():
            headers = {
                'Cookie':'{}'.format(payload)
            }
            more("检测载荷[cookie]:{},{}".format(self.url,headers['Cookie']))
            req = self.Send(url=self.url,method=self.get,headers=headers)
            if search(payload,req.content):
                plus("疑似Cookie头信息中存在XSS漏洞:")
                more("URL[地址]: {}".format(req.url))
                more("PAYLOAD[有效载荷]: {}".format(payload))
                isNothing = False
                if self.result['headerxss']:
                    self.result['headerxss'] = "[cookie],{}".format(payload)
                else:
                    self.result['headerxss'] += " [cookie],{}".format(payload)
                break
        if isNothing:
            info_nothing()
    def referer(self):
        isNothing = True
        for payload in pxss():
            headers = {
                'Referer':'{}'.format(payload)
            }
            more("检测载荷[Referer]:{},{}".format(self.url,headers['Referer']))
            req = self.Send(url=self.url,method=self.get,headers=headers)
            if search(payload,req.content):
                plus("疑似Referer头信息中存在XSS漏洞:")
                more("URL[地址]: {}".format(req.url))
                more("PAYLOAD[有效载荷]: {}".format(payload))
                isNothing = False
                if self.result['headerxss']:
                    self.result['headerxss'] = "[referer],{}".format(payload)
                else:
                    self.result['headerxss'] += " [referer],{}".format(payload)
                break
        if isNothing:
            info_nothing()
    def useragent(self):
        isNothing = True
        for payload in pxss():
            headers = {
                'User-Agent':'{}'.format(payload)
            }
            more("检测载荷[User-Agent]:{},{}".format(self.url,headers['User-Agent']))
            req = self.Send(url=self.url,method=self.get,headers=headers)
            if search(payload,req.content):
                plus("疑似User-Agent头信息中存在XSS漏洞:")
                more("URL[地址]: {}".format(req.url))
                more("PAYLOAD[有效载荷]: {}".format(payload))
                if self.result['headerxss']:
                    self.result['headerxss'] = "[useragent],{}".format(payload)
                else:
                    self.result['headerxss'] += " [useragent],{}".format(payload)
                isNothing = False
                break
        if isNothing:
            info_nothing()
    def check(self):
        info("检测Http头信息[Cookie,Referer,UserAgent]XSS漏洞:")
        self.cookie()
        self.referer()
        self.useragent()
        return self.result
        
def run(kwargs,url,data):
    result = {}
    scan = headerxss(kwargs,url,data)
    result = scan.check()
    return result