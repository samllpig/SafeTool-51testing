#!/usr/bin/env python
# -*- coding:utf-8 -*-

from re import search,I
from lib.utils.params import *
from lib.utils.printer import *
from lib.request.request import *
from lib.utils.payload import *

class htmli(Request):
    """
    html 代码注入
    """
    get = "GET"
    post = "POST"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'htmli':None
        }
    
    def check(self):
        info("检测html代码注入漏洞...")
        URL = None
        DATA = None
        PAYLOAD = None
        isNothing = True
        for payload in html():
            if self.data:
                addpayload = padd(self.url,payload,self.data)
                for data in addpayload.run():
                    more("检测载荷:{},{}".format(self.url,data))
                    req = self.Send(url=self.url,method=self.post,data=data)
                    if search(payload,req.content):
                        URL = req.url
                        DATA = data
                        PAYLOAD = payload
                        break
            else:
                urls = padd(self.url,payload,None)
                for url in urls.run():
                    more("检测载荷:{}".format(url))
                    req = self.Send(url=url,method=self.get)
                    if search(payload,req.content):
                        URL = url
                        PAYLOAD = payload
                        break
            if URL and PAYLOAD:
                if DATA != None:
                    plus("疑似存在HTML代码注入:")
                    more("URL[地址]: {}".format(URL))
                    more("POST DATA[数据]: {}".format(DATA))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['htmli'] = PAYLOAD
                    isNothing = False
                elif DATA == None:
                    plus("疑似存在HTML代码注入:")
                    more("URL[地址]: {}".format(URL))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['htmli'] = PAYLOAD
                    isNothing = False
                break
        if isNothing:
            info_nothing()
        return self.result
def run(kwargs,url,data):
    result = {}
    scan = htmli(kwargs,url,data)
    result = scan.check()
    return result

