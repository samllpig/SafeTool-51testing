#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os

def dirs(path):
    files = []
    _ = os.listdir(path)
    for file in _:
        if not file.endswith('.py'):
            continue
        if file == '__init__.py':
            continue
        files.append(file)
    return files