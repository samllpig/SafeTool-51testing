#!/usr/bin/env python
# -*- coding:utf-8 -*-

g_exceptions = []

def print_exceptions():
    global g_exceptions
    for e in g_exceptions:
        print("异步执行异常: {}".format(e))

def handler_exception(loop, context):
    global g_exceptions
    if 'exception' in context:
        g_exceptions.append(context['exception'])
    else:
        g_exceptions.append(context)        